import { useQuery, gql } from "@apollo/client";
import "./App.css";

const GET_LANGUAGES = gql`
  query Query {
    languages
  }
`;

function App() {
  const { loading, error, data } = useQuery(GET_LANGUAGES);

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  return (
    <ul>
      {data.languages.map((lang) => (
        <li>{lang}</li>
      ))}
    </ul>
  );
}

export default App;
