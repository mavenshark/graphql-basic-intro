const http = require("http");
const express = require("express");
const { ApolloServer, gql } = require("apollo-server-express");
const { execute, subscribe } = require("graphql");
const { SubscriptionServer } = require("subscriptions-transport-ws");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { PubSub } = require("graphql-subscriptions");
const { type } = require("os");

const pubsub = new PubSub();
let langs = ["Python", "Java"];

const typeDefs = gql`
  type Query {
    languages: [String]
  }

  type Mutation {
    createLanguage(name: String): [String]
  }

  type Subscription {
    languageCreated: [String]
  }
`;

const resolvers = {
  Query: {
    languages() {
      return langs;
    },
  },
  Mutation: {
    createLanguage(_, args) {
      langs = [...langs, args.name];
      pubsub.publish("LANGUAGE_CREATED", { languageCreated: langs });
      return langs;
    },
  },
  Subscription: {
    languageCreated: {
      subscribe: () => pubsub.asyncIterator(["LANGUAGE_CREATED"]),
    },
  },
};

async function startApolloServer(typeDefs, resolvers) {
  const app = express();
  const httpServer = http.createServer(app);

  const schema = makeExecutableSchema({ typeDefs, resolvers });

  const server = new ApolloServer({
    schema,
    plugins: [
      {
        async serverWillStart() {
          return {
            async drainServer() {
              subscriptionServer.close();
            },
          };
        },
      },
    ],
    introspection: true,
  });

  await server.start();
  server.applyMiddleware({
    app,
    path: "/",
  });

  const subscriptionServer = SubscriptionServer.create(
    {
      schema,
      execute,
      subscribe,
    },
    {
      server: httpServer,
      path: server.graphqlPath,
    }
  );

  httpServer.listen(4000, () => {
    console.log(
      `🚀 Server ready at http://localhost:4000${server.graphqlPath}`
    );
    console.log(
      `🚀 Subscription endpoint ready at ws://localhost:4000${server.graphqlPath}`
    );
  });
}

startApolloServer(typeDefs, resolvers);
